---
title: Term Dates 2020/21
index: 31
---

School Session Dates 2020/21

|              | Day     | Date            |
| -----        | ------- | --------------- |
| Staff Resume | Monday  | 17 August 2020† |
|              | Tuesday | 18 August 2020† |
| Pupils Resume | Wednesday | 19 August 2020 |
| P1s Start|Thursday|20 August 2020
|Autumn Holiday, School closed|Monday|21 September 2020
|All Resume|Tuesday|22 September 2020
|Mid-Term, All break|Friday|16 October 2020
|Staff in-service|Tuesday|27 October 2020†
|Pupils resume|Wednesday|28 October 2020
|Term ends|Friday|18 December 2020
|Staff Resume|Tuesday|5 January 2021†
|Pupils resume|Wednesday|6 January 2021
|Mid-term, All break|Friday|5 February 2021
|All resume|Tuesday|16 February 2021
|Term ends|Thursday|1 April 2021
|Staff In service|Tuesday|20 April 2021†
|Pupils resume|Wednesday|21 April 2021
|May Holiday, All break|Friday|14 May 2021
|Staff Resume|Friday|21 May 2021†
|Pupils resume|Monday|24 May 2021
|Term ends|Friday|25 June 2021

† In-service days
